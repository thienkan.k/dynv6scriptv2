# This file is based on https://gist.github.com/corny/7a07f5ac901844bd20c9 and https://gist.github.com/pulsar256/42313fcb2d3ae805805f
# I modified this from the old file and share it with my friends for use in our "mini" project.

token=6U-xdyH_spDqtjwzf5JN46iJ3McUce
hostname=falling.dynv6.net

if [ -z "$hostname" -o -z "$token" ]; then
  echo "Usage: token=<your-authentication-token> $0 your-name.dynv6.net [device]"
  exit 1
fi

v4_address=$(ip -4 addr list $device | grep "global" | sed -n 's/.*inet \([0-9.]\+\).*/\1/p' | head -n 1);

if [ -e /usr/bin/curl ]; then
  bin="curl -fsS"
elif [ -e /usr/bin/wget ]; then
  bin="wget -O-"
else
  echo "neither curl nor wget found"
  exit 1
fi

if [ -z "$v4_address" ]; then
  echo "no IPv4 address found"
  exit 1
fi

echo "new ipv4 address detected ${v4_address}, updating"
# send addresses to dynv6
$bin "http://ipv4.dynv6.com/api/update?hostname=$hostname&ipv4=$v4_address&token=$token"
